﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Web;

namespace hashgenerator
{
    public partial class Form1 : Form
    {   
        /* 
         * key1 and key2 are the keys to encrypt
         * pKey1 and Pkey2 are byte of key1 and key2
         */
        private string key1;
        private string key2;
        private byte[] pKey1 = new byte[8];
        private byte[] pKey2 = new byte[8];

        // the IV of LM
        private static byte[] IV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 }; 
        private byte[] md4Hash=new byte[16];

        public Form1()
        {
            InitializeComponent();
        }

        private void password_TextChanged(object sender, EventArgs e)
        {   
            //detect the the change of the text
            string lmHash, ntHash;
            lmHash = dealLm(this.password.Text);
            ntHash = dealNt(this.password.Text);
            this.lmHash.Text = lmHash;
            this.ntHash.Text = ntHash;
        }

        public string dealLm(string p_word)
        {
            ///make the password to upper
            string temp3="",temp = p_word.ToUpper();
            char[] zero = new char[8] { '0', '0', '0', '0', '0', '0', '0', '0' };
            ///if the length is larger than 14 
            if (temp.Length >= 14)
            {
                temp = temp.Substring(0, 14);
                for (int i = 0; i < 14; i++)
                {
                    string re=new string(zero,0,8-(Convert.ToString((int)temp[i], 2)).Length);
                    temp3 = temp3 + re + Convert.ToString((int)temp[i], 2);
                }
            }
            /// the length less than 14 ,fill with 0x00
            else
            {
                int remainder = 0;
                remainder = 14 - temp.Length;
                for (int i = 0; i < temp.Length; i++)
                {
                    string re = new string(zero, 0, 8 - (Convert.ToString((int)temp[i], 2)).Length);
                    temp3 = temp3 + re + Convert.ToString((int)temp[i], 2);
                }
                for (int i = 0; i < remainder; i++)
                    temp3 += "00000000";
            }

            ///str_toA_key
            str_to_key(temp3);

            ///there is a bug ,that you cant use 000000000000000 to encrypt
            if (key1 == "0000000000000000")
                key1 = "AAD3B435B51404EE";
            else
                key1 = EncryptData(pKey1, "KGS!@#$%");
            if (key2 == "0000000000000000")
                key2 = "AAD3B435B51404EE";
            else
                key2 = EncryptData(pKey2, "KGS!@#$%");
            return key1+key2;
        }

        public void str_to_key(string word)
        {   
            key1="";
            key2="";
            string[] myStr=new string[16];
            for (int i = 0; i < 16; i++)
            {
                myStr[i] = word.Substring(i * 7, 7) + "0";
                if (i <= 7)
                {
                    key1 += (Convert.ToInt32(myStr[i], 2)).ToString("X2");
                    pKey1[i] = (byte)(Convert.ToInt32(myStr[i], 2));
                }
                else
                {
                    key2 += (Convert.ToInt32(myStr[i], 2)).ToString("X2");
                    pKey2[i-8] = (byte)(Convert.ToInt32(myStr[i], 2));
                }
            }
        }

        public string dealNt(string p_word)
        {   
            byte[] buffer=new byte[2*p_word.Length];
            for (int i = 0; i < p_word.Length; i++)
            {
                buffer[2 * i] =(byte)((int)p_word[i]);
                buffer[2*i+1]=0;
            }
            using (HashAlgorithm hash = new MD4())
            {
                md4Hash=hash.ComputeHash(buffer);
            }
            /* 
             *  if you use md5 that will be easy
             *  HashAlgorithm a = new MD5CryptoServiceProvider();
             *   md4Hash = a.ComputeHash(buffer);
             *   */
            StringBuilder ret = new StringBuilder();
            foreach (byte b in md4Hash)
            {
                ret.AppendFormat("{0:X2}", b);
            }
            ret.ToString();
            return ret.ToString();
        }

        public static string EncryptData(byte[] key, string str)
        {
            byte[] bIV = IV;
            byte[] data = Encoding.Default.GetBytes(str);
            try
            {
                DESCryptoServiceProvider desc = new DESCryptoServiceProvider();
                //兼容其他语言的Des加密算法  
                desc.Mode = CipherMode.ECB;
                //自动补0  
                desc.Padding = PaddingMode.Zeros;
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, desc.CreateEncryptor(key, bIV), CryptoStreamMode.Write);
                cStream.Write(data, 0, data.Length);
                cStream.FlushFinalBlock();
                StringBuilder ret = new StringBuilder();
                foreach (byte b in mStream.ToArray())
                {
                    ret.AppendFormat("{0:X2}", b);
                }
                ret.ToString();
                return ret.ToString();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// the exit button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



    }

    //class md4
    public class MD4 : HashAlgorithm
    {
        private uint _a;
        private uint _b;
        private uint _c;
        private uint _d;
        private uint[] _x;
        private int _bytesProcessed;

        public MD4()
        {
            _x = new uint[16];

            Initialize();
        }

        public override void Initialize()
        {
            _a = 0x67452301;
            _b = 0xefcdab89;
            _c = 0x98badcfe;
            _d = 0x10325476;

            _bytesProcessed = 0;
        }

        protected override void HashCore(byte[] array, int offset, int length)
        {
            ProcessMessage(Bytes(array, offset, length));
        }

        protected override byte[] HashFinal()
        {
            try
            {
                ProcessMessage(Padding());

                return new[] { _a, _b, _c, _d }.SelectMany(word => Bytes(word)).ToArray();
            }
            finally
            {
                Initialize();
            }
        }

        private void ProcessMessage(IEnumerable<byte> bytes)
        {
            foreach (byte b in bytes)
            {
                int c = _bytesProcessed & 63;
                int i = c >> 2;
                int s = (c & 3) << 3;

                _x[i] = (_x[i] & ~((uint)255 << s)) | ((uint)b << s);

                if (c == 63)
                {
                    Process16WordBlock();
                }

                _bytesProcessed++;
            }
        }

        private static IEnumerable<byte> Bytes(byte[] bytes, int offset, int length)
        {
            for (int i = offset; i < length; i++)
            {
                yield return bytes[i];
            }
        }

        private IEnumerable<byte> Bytes(uint word)
        {
            yield return (byte)(word & 255);
            yield return (byte)((word >> 8) & 255);
            yield return (byte)((word >> 16) & 255);
            yield return (byte)((word >> 24) & 255);
        }

        private IEnumerable<byte> Repeat(byte value, int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return value;
            }
        }

        private IEnumerable<byte> Padding()
        {
            return Repeat(128, 1)
               .Concat(Repeat(0, ((_bytesProcessed + 8) & 0x7fffffc0) + 55 - _bytesProcessed))
               .Concat(Bytes((uint)_bytesProcessed << 3))
               .Concat(Repeat(0, 4));
        }

        private void Process16WordBlock()
        {
            uint aa = _a;
            uint bb = _b;
            uint cc = _c;
            uint dd = _d;

            foreach (int k in new[] { 0, 4, 8, 12 })
            {
                aa = Round1Operation(aa, bb, cc, dd, _x[k], 3);
                dd = Round1Operation(dd, aa, bb, cc, _x[k + 1], 7);
                cc = Round1Operation(cc, dd, aa, bb, _x[k + 2], 11);
                bb = Round1Operation(bb, cc, dd, aa, _x[k + 3], 19);
            }

            foreach (int k in new[] { 0, 1, 2, 3 })
            {
                aa = Round2Operation(aa, bb, cc, dd, _x[k], 3);
                dd = Round2Operation(dd, aa, bb, cc, _x[k + 4], 5);
                cc = Round2Operation(cc, dd, aa, bb, _x[k + 8], 9);
                bb = Round2Operation(bb, cc, dd, aa, _x[k + 12], 13);
            }

            foreach (int k in new[] { 0, 2, 1, 3 })
            {
                aa = Round3Operation(aa, bb, cc, dd, _x[k], 3);
                dd = Round3Operation(dd, aa, bb, cc, _x[k + 8], 9);
                cc = Round3Operation(cc, dd, aa, bb, _x[k + 4], 11);
                bb = Round3Operation(bb, cc, dd, aa, _x[k + 12], 15);
            }

            unchecked
            {
                _a += aa;
                _b += bb;
                _c += cc;
                _d += dd;
            }
        }

        private static uint ROL(uint value, int numberOfBits)
        {
            return (value << numberOfBits) | (value >> (32 - numberOfBits));
        }

        private static uint Round1Operation(uint a, uint b, uint c, uint d, uint xk, int s)
        {
            unchecked
            {
                return ROL(a + ((b & c) | (~b & d)) + xk, s);
            }
        }

        private static uint Round2Operation(uint a, uint b, uint c, uint d, uint xk, int s)
        {
            unchecked
            {
                return ROL(a + ((b & c) | (b & d) | (c & d)) + xk + 0x5a827999, s);
            }
        }

        private static uint Round3Operation(uint a, uint b, uint c, uint d, uint xk, int s)
        {
            unchecked
            {
                return ROL(a + (b ^ c ^ d) + xk + 0x6ed9eba1, s);
            }
        }
    }
}
